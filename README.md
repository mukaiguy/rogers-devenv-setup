# My Development Enviroment Setup
My preferences for a comfortable development environment and also the resources I use the most.
If some wants to use my experiances to help them lear thats great!! Just keep in mind I dont privide support. 
Search online for anything you don't understand and use a few resources to compare notes.

### Note About the OS                                                                                                                                             
For Linux people, Ubuntu is my primary envrioment but it should mostly work on Debian/Rasibian too

For Apple People, If you're on a Mac running macOSX 10.14.6 or higher most of my notes should work. If you haven't already install [Homebrew](https://brew.sh/)
 
## Terminal Enviroment                                                                                                                                                                                                                                                                                                            
### Install:                                                                                                                                                                                                                                                                                                                
 ZSH: Not inluded on linux but it is with OSX
 ```bash
sudo apt install zsh
```                                                                                                                                                                                                                                                                                          
   #### oh-my-zsh:                                                                                                                                                                                                                                                                                                    
```bash
            
# For OSX and Liniux
# First run to install oh-my-zsh or visit their website for instructions 
            
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" 
            
# For Linux
    sudo apt install fzf
            
# For OSX
    brew install fzf
            
# For OSX and Liniux 
# Once complete type the following command to enter the settings/config preferances I use nano of basic tasks and its built in on Linux and OSX
          
    sudo nano ~/.zshrc
            
    # This will open the nano editor, NOTE to exit nano use control+x and read any prompts at the bottom of the screen.
    # From here change your desiered settings and for my setting see the ZSH CONFIG Section

```
           
#### ZSH CONFIG (.zshrc file): copy & paste from the linked repository.                                                                                           
[.zshrc settings](https://github.com/MukaiGuy/Rogers_Development-Enviroment-Setup/blob/master/.zshrc-configs)
[SSH Configs](https://gitlab.com/mukaiguy/keys)

### Installing Python and Other Techologies on Linux    

Python 3.8 ` sudo apt install python  `
PIP tools ` sudo apt install python3-venv python3-pip `

